package com.hieptran.hellogit;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;

import com.hieptran.hellogit.UI.Home.HomeFragment;
import com.hieptran.hellogit.UI.Music.MusicFragment;
import com.hieptran.hellogit.UI.PlayMusic.PlayMusicFragment;
import com.hieptran.hellogit.UI.Profile.ProfileFragment;

import com.hieptran.hellogit.service.PlaySongService;
import com.roughike.bottombar.BottomBar;

public class MainActivity extends AppCompatActivity {

    public static final String DEBUG_FLAG = "APP_HIEP";
    BottomBar bottomBar;
    HomeFragment homeFragment;
    MusicFragment musicFragment;
    ProfileFragment profileFragment;
    private PlayMusicFragment playMusicFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mappingView();
        initializationObject();

        bottomBar.setOnTabSelectListener(tabId -> {

            if (tabId == R.id.tab_home) {
                transFragment(homeFragment);
            } else if (tabId == R.id.tab_music) {
                transFragment(musicFragment);
            } else {
                transFragment(profileFragment);
            }

        });

    }

    private void transFragment(Fragment fragment) {
        FragmentTransaction fts = getSupportFragmentManager().beginTransaction();
        fts.replace(R.id.contentContainer, fragment);
        fts.addToBackStack(null);
        fts.commit();
    }


    private void initializationObject() {
        homeFragment = new HomeFragment();
        musicFragment = new MusicFragment();
        profileFragment = new ProfileFragment();
        playMusicFragment = new PlayMusicFragment();
    }

    private void mappingView() {
        bottomBar = findViewById(R.id.bottomBar);

    }
}
