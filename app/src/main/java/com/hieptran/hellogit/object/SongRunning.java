package com.hieptran.hellogit.object;

public class SongRunning {
    private long timeSongRunning = 0;
    private boolean prepareSongDone = false;

    public SongRunning() {
    }

    public SongRunning(long timeSongRunning, boolean prepareSongDone) {
        this.timeSongRunning = timeSongRunning;
        this.prepareSongDone = prepareSongDone;
    }

    public long getTimeSongRunning() {
        return timeSongRunning;
    }

    public void setTimeSongRunning(long timeSongRunning) {
        this.timeSongRunning = timeSongRunning;
    }

    public boolean isPrepareSongDone() {
        return prepareSongDone;
    }

    public void setPrepareSongDone(boolean prepareSongDone) {
        this.prepareSongDone = prepareSongDone;
    }
}
