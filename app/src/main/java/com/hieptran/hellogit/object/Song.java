package com.hieptran.hellogit.object;

import com.google.gson.annotations.SerializedName;

public class Song {
    @SerializedName("name")
    private String name;

    @SerializedName("type")
    private String type;

    @SerializedName("date")
    private String date;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Song(String name, String type, String date, String link) {
        this.name = name;
        this.type = type;
        this.date = date;
        this.link = link;
    }

    @SerializedName("link")
    private String link;

    public Song() {
    }


}
