package com.hieptran.hellogit.model;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

import com.hieptran.hellogit.object.Song;
import com.hieptran.hellogit.object.SongRunning;

public class Repository {

    public static final Repository INSTANCE = new Repository();

    private MediatorLiveData<SongRunning> mData = new MediatorLiveData<>();

    private Repository() {
    }

    public static Repository getInstance() {
        return INSTANCE;
    }

    public LiveData<SongRunning> getData() {
        return mData;
    }

    public void addDataSource(LiveData<SongRunning> data) {
        mData.addSource(data, mData::setValue);
    }

    public void removeDataSource(LiveData<Integer> data) {
        mData.removeSource(data);
    }

}
