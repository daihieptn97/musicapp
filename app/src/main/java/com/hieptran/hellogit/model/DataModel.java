package com.hieptran.hellogit.model;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class DataModel extends ViewModel {
    // TODO: Implement the ViewModel
    private MutableLiveData<String> data = new MutableLiveData<>();

    public void setData(String s) {
        data.setValue(s);
    }

    public LiveData<String> getData() {
        return data;
    }

}
