package com.hieptran.hellogit.model;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.hieptran.hellogit.object.Song;
import com.hieptran.hellogit.object.SongRunning;

public class SongModel extends ViewModel {
    public LiveData<SongRunning> getData() {
        // for simplicity return data directly to view
        return Repository.getInstance().getData();
    }
}
