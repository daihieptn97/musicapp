package com.hieptran.hellogit.service;

import android.app.Service;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.gauravk.audiovisualizer.visualizer.CircleLineVisualizer;
import com.gauravk.audiovisualizer.visualizer.WaveVisualizer;
import com.hieptran.hellogit.MainActivity;
import com.hieptran.hellogit.R;
import com.hieptran.hellogit.UI.Home.HomeFragment;
import com.hieptran.hellogit.UI.PlayMusic.PlayMusicModel;
import com.hieptran.hellogit.object.Song;
import com.hieptran.hellogit.object.SongRunning;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class PlaySongService extends Service {
    public static MediaPlayer mediaPlayer;
    // private SeekBar seekbarMusic;
    private AudioManager mAudioManager;
    private TextView txtStartTimeSong, txtEndTimeSong;

    private Handler handler;
    private boolean prepareSongDone = false;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public static final MutableLiveData<SongRunning> mData = new MutableLiveData<>();

    public static MutableLiveData<SongRunning> getData() {
        return mData;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        initialization();
        initializationMediaPlayer(intent.getStringExtra(HomeFragment.KEY_LINK));

        CreateNotification.createNotification(this, R.drawable.ic_play_arrow_24dp);
//        this.startForeground();
        return START_NOT_STICKY;
    }

    private void initializationMediaPlayer(String link) {
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

        mediaPlayer.setOnPreparedListener(mediaPlayer -> {
            prepareSongDone = true;
            mData.setValue(new SongRunning(mediaPlayer.getCurrentPosition(), prepareSongDone));
            // changerSeekBar();
        });

        try {
            mediaPlayer.setDataSource(link);
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void initialization() {
        mediaPlayer = new MediaPlayer();
        handler = new Handler();
    }


}
