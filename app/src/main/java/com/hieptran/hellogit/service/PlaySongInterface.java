package com.hieptran.hellogit.service;

public interface PlaySongInterface {
    void onTrackPrevious();

    void onTrackPlay();

    void onTrackPause();

    void onTrackNext();
}
