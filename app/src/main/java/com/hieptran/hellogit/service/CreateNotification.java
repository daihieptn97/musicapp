package com.hieptran.hellogit.service;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.media.MediaDescriptionCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.MediaSessionCompat;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.hieptran.hellogit.R;
import com.hieptran.hellogit.object.Song;

public class CreateNotification {
    public static final String CHANNEL_ID = "channel1";

    public static final String ACTION_PREVIUOS = "actionprevious";
    public static final String ACTION_PLAY = "actionplay";
    public static final String ACTION_NEXT = "actionnext";

    public static void createNotification(Service context, int btnPlay) {

        Bitmap albumArtBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.assassins);

        MediaSessionCompat mediaSessionCompat = new MediaSessionCompat(context, "tag");

        MediaControllerCompat controller = mediaSessionCompat.getController();
        MediaMetadataCompat mediaMetadata = controller.getMetadata();
        MediaDescriptionCompat description = mediaMetadata.getDescription();

        Intent snoozeIntent = new Intent(context, PlayBroadcastReceiver.class);
        snoozeIntent.setAction(ACTION_PLAY);
        snoozeIntent.putExtra("actionname", 1);
        PendingIntent pendingIntentPLay =
                PendingIntent.getBroadcast(context, 0, snoozeIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification = new NotificationCompat.Builder(context, CHANNEL_ID)
                // Show controls on lock screen even when user hides sensitive content.
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setSmallIcon(R.drawable.ic_beach_access)

                // Enable launching the player by clicking the notification
                .setContentIntent(controller.getSessionActivity())

                // Add media control buttons that invoke intents in your media service
                .addAction(R.drawable.ic_skip_previous_24dp, "Previous", pendingIntentPLay) // #0
                .addAction(R.drawable.ic_play_arrow_24dp, "Play", pendingIntentPLay)  // #1
                .addAction(R.drawable.ic_skip_next_24dp, "Next", pendingIntentPLay)     // #2
                // Apply the media style template

                .setStyle(new androidx.media.app.NotificationCompat.MediaStyle()
                        .setShowActionsInCompactView(1 /* #1: pause button */)
                        .setMediaSession(mediaSessionCompat.getSessionToken()))

                .setContentTitle(description.getTitle())
                .setContentText(description.getSubtitle())
                .setSubText(description.getDescription())


                .setLargeIcon(albumArtBitmap)
                .build();

        context.startForeground(1, notification);

    }
}
