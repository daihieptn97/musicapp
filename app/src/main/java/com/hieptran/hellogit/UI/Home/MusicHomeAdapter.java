package com.hieptran.hellogit.UI.Home;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.hieptran.hellogit.R;
import com.hieptran.hellogit.object.Song;

import java.util.List;

public class MusicHomeAdapter extends RecyclerView.Adapter<MusicHomeAdapter.ViewHolder> {
    private Context context;
    private List<Song> mData;
    private ListenerClickItemList mListenerClickItemList;

    public MusicHomeAdapter(Context context, List<Song> mData) {
        this.context = context;
        this.mData = mData;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.item_mucsic_recycle_home, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtName.setText(mData.get(position).getName());
        holder.txtDuration.setText(mData.get(position).getDate());
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // each data item is just a string in this case
        TextView txtName, txtDuration;
        AlphaAnimation buttonClick = new AlphaAnimation(1F, 0.2F);
        public ViewHolder(View v) {
            super(v);
            txtDuration = v.findViewById(R.id.itemRecycleHomeDuration);
            txtName = v.findViewById(R.id.itemRecycleHomeName);

            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mListenerClickItemList.onClick(v, getAdapterPosition());
            buttonClick.setStartTime(0);
            buttonClick.setDuration(250);
            v.startAnimation(buttonClick);

        }

    }

    public void setClickListener(ListenerClickItemList mListenerClickItemList) {
        this.mListenerClickItemList = mListenerClickItemList;
    }

    public interface ListenerClickItemList {
        void onClick(View v, int position);
    }


}
