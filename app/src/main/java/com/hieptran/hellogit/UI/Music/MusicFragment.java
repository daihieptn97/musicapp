package com.hieptran.hellogit.UI.Music;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hieptran.hellogit.R;
import com.hieptran.hellogit.model.DataModel;

public class MusicFragment extends Fragment {


    private TextView txtTextMusic;
    private DataModel model;

    public MusicFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_music, container, false);

        mappingView(v);
        initialization();

        model.getData().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(String s) {
                txtTextMusic.setText(s);
            }
        });

        return v;
    }

    private void initialization() {
        model = new ViewModelProvider(requireActivity()).get(DataModel.class);
    }

    private void mappingView(View v) {
        txtTextMusic = v.findViewById(R.id.txt_name);
    }
}
