package com.hieptran.hellogit.UI.Home;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hieptran.hellogit.R;
import com.hieptran.hellogit.UI.PlayMusic.PlayMusicFragment;
import com.hieptran.hellogit.UI.PlayMusic.PlayMusicModel;
import com.hieptran.hellogit.clinet.APIClient;
import com.hieptran.hellogit.model.DataModel;
import com.hieptran.hellogit.model.Repository;
import com.hieptran.hellogit.object.JsonObject;
import com.hieptran.hellogit.object.Song;
import com.hieptran.hellogit.service.PlaySongService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment {

    private DataModel mViewModel;

    private RecyclerView rcvListMusic;
    private MusicHomeAdapter adapter;

    PlayMusicModel musicModel;
    private List<Song> songs = null;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.home_fragment, container, false);

        mappingView(v);
        initialization();
        getData();

        rcvListMusic = v.findViewById(R.id.rcHome);
        songs = new ArrayList<>();

        rcvListMusic.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new MusicHomeAdapter(getContext(), songs);
        rcvListMusic.setAdapter(adapter);

        adapter.setClickListener((v1, position) -> {

            Intent intent = new Intent(getActivity(), PlaySongService.class);
            intent.putExtra(KEY_LINK, songs.get(position).getLink());
            getContext().startService(intent);

            Repository.getInstance().addDataSource(PlaySongService.getData());

            musicModel.setSongRunning(songs.get(position));
            getActivity()
                    .getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.contentContainer, new PlayMusicFragment())
                    .addToBackStack(null)
                    .commit();

        });
        return v;
    }

    public static String KEY_LINK = "link";


    private void getData() {
        APIClient.getRetrofit().create(APIClient.APIInterface.class).getAllMusic().enqueue(new Callback<JsonObject<Song>>() {
            @Override
            public void onResponse(Call<JsonObject<Song>> call, Response<JsonObject<Song>> response) {
                if (response.isSuccessful()) {

                    songs.addAll(response.body().getData());
                    //adapter.notifyItemRangeInserted(0, musics.size());
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<JsonObject<Song>> call, Throwable t) {

            }
        });
    }

    private void initialization() {
        musicModel = new ViewModelProvider(requireActivity()).get(PlayMusicModel.class);
    }

    private void mappingView(View v) {
//        btn1 = v.findViewById(R.id.btnHome);
//        txtTextHome = v.findViewById(R.id.txtHome);
    }
}
