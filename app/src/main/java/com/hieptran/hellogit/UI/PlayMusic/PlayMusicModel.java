package com.hieptran.hellogit.UI.PlayMusic;

import android.media.MediaPlayer;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.hieptran.hellogit.object.Song;

public class PlayMusicModel extends ViewModel {
    private MutableLiveData<Song> songRunning = new MutableLiveData<>();
    private MutableLiveData<Boolean> isRunningMutableLiveData = new MutableLiveData<>();
    private MutableLiveData<MediaPlayer> mediaPlayerMutableLiveData = new MutableLiveData<>();


    public MutableLiveData<Boolean> getIsRunningMutableLiveData() {
        return isRunningMutableLiveData;
    }

    public MutableLiveData<MediaPlayer> getMediaPlayerMutableLiveData() {
        return mediaPlayerMutableLiveData;
    }

    public void setIsRunningMutableLiveData(Boolean isRunningMutableLiveData) {
        this.isRunningMutableLiveData.setValue(isRunningMutableLiveData);
    }

    public void setMediaPlayerMutableLiveData(MediaPlayer mediaPlayerMutableLiveData) {
        this.mediaPlayerMutableLiveData.setValue(mediaPlayerMutableLiveData);
    }

    public MutableLiveData<Song> songRunning() {
        return songRunning;
    }

    public void setSongRunning(Song songRunning) {
        this.songRunning.setValue(songRunning);
    }
}
