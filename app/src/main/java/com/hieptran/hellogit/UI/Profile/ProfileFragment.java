package com.hieptran.hellogit.UI.Profile;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.hieptran.hellogit.R;
import com.hieptran.hellogit.model.DataModel;

import java.util.Random;

public class ProfileFragment extends Fragment {

    private TextView txtProfile;
    private DataModel model;
    private Button btnButton1;

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_profile, container, false);

        mappingView(v);
        initialization();

        model.getData().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(String s) {
                txtProfile.setText(s);
            }
        });


        btnButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                model.setData((new Random().nextInt(1000) + 1000) + "");
            }
        });

        return v;
    }

    private void initialization() {
        model = new ViewModelProvider(requireActivity()).get(DataModel.class);
    }

    private void mappingView(View v) {
        btnButton1 = v.findViewById(R.id.btnProfile);
        txtProfile = v.findViewById(R.id.txtProfile);
    }
}
