package com.hieptran.hellogit.UI.PlayMusic;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;


import com.gauravk.audiovisualizer.visualizer.CircleLineVisualizer;
import com.gauravk.audiovisualizer.visualizer.WaveVisualizer;
import com.hieptran.hellogit.MainActivity;
import com.hieptran.hellogit.R;
import com.hieptran.hellogit.model.SongModel;
import com.hieptran.hellogit.object.Song;
import com.hieptran.hellogit.object.SongRunning;
import com.hieptran.hellogit.service.CreateNotification;
import com.hieptran.hellogit.service.PlaySongInterface;
import com.hieptran.hellogit.service.PlaySongService;

import java.text.SimpleDateFormat;
import java.util.Date;

public class PlayMusicFragment extends Fragment implements View.OnClickListener, PlaySongInterface {

    private TextView txtName;


    private ImageButton btnPrevious;
    private ImageButton btnNext;
    private ImageButton btnPlay;
    private ImageButton btnDownload;
    private ImageButton btnShuffle;
    private SeekBar seekbarVolume;

    private WaveVisualizer blastVisualizer;
    // private MediaPlayer mediaPlayer;
    private CircleLineVisualizer circleLineVisualizer;
    private SeekBar seekbarMusic;
    private AudioManager mAudioManager;
    private TextView txtStartTimeSong, txtEndTimeSong;

    private Handler handler;
    private PlayMusicModel musicModel;
    SongModel songModel;
    Song songRunning;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.play_music, container, false);

        mappingView(view);
        initialization();

        musicModel.songRunning().observe(getViewLifecycleOwner(), song -> {
            txtName.setText(song.getName());
            doSeekBarVolume();
            songRunning = song;
            //doSeekBarMusic();
        });


        songModel = new ViewModelProvider(requireActivity()).get(SongModel.class);
        songModel.getData().observe(getViewLifecycleOwner(), songRunning -> {


            if (songRunning.isPrepareSongDone()) {
                if (txtEndTimeSong.getText().toString().equals("00:00")) {
                    seekbarMusic.setMax(PlaySongService.mediaPlayer.getDuration());
                    txtEndTimeSong.setText(convertTime(PlaySongService.mediaPlayer.getDuration()));
                    circleLineVisualizer.setAudioSessionId(PlaySongService.mediaPlayer.getAudioSessionId());
                }
                changerSeekBar();
                btnPlay.setImageResource(R.drawable.ic_pause_24dp);
                onTrackPlay();
            }
        });

        seekbarMusic.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (b) {
                    if (PlaySongService.mediaPlayer != null) {
                        PlaySongService.mediaPlayer.seekTo(i);
                    } else {
                        Toast.makeText(getContext(), "Pending ...", Toast.LENGTH_SHORT).show();
                    }

                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                if (PlaySongService.mediaPlayer != null) {
                    PlaySongService.mediaPlayer.pause();
                } else {
                    Toast.makeText(getContext(), "Pending ...", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (PlaySongService.mediaPlayer != null) {
                    PlaySongService.mediaPlayer.start();
                } else {
                    Toast.makeText(getContext(), "Pending ...", Toast.LENGTH_SHORT).show();
                }

            }
        });
        createChanel();
        getActivity().registerReceiver(broadcastReceiver, new IntentFilter("TRACKS_TRACKS"));

        return view;
    }

    private String convertTime(long time) {
        return (new SimpleDateFormat("mm:ss")).format(new Date(time));
    }

    private void doSeekBarVolume() {

        seekbarVolume.setMax(15);
        seekbarVolume.setProgress(mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
        seekbarVolume.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
//                Toast.makeText(getContext(), "seekbar progress: " + i, Toast.LENGTH_SHORT).show();
                Log.d(MainActivity.DEBUG_FLAG, i + "");
                mAudioManager.setStreamVolume(
                        AudioManager.STREAM_MUSIC, // Stream type
                        i, // Index
                        AudioManager.FLAG_SHOW_UI // Flags
                );
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                //Toast.makeText(getContext(), "seekbar touch started!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // Toast.makeText(getContext(), "seekbar touch stopped!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void changerSeekBar() {
        txtStartTimeSong.setText(convertTime(PlaySongService.mediaPlayer.getCurrentPosition()));
        seekbarMusic.setProgress(PlaySongService.mediaPlayer.getCurrentPosition());
        if (PlaySongService.mediaPlayer.isPlaying()) {
            Runnable runnable = this::changerSeekBar;
            handler.postDelayed(runnable, 1000);
        }
    }

    private void createChanel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(CreateNotification.CHANNEL_ID, "name", NotificationManager.IMPORTANCE_LOW);
            NotificationManager notificationManager = getContext().getSystemService(NotificationManager.class);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }
        }
    }

    private void initialization() {

        btnPlay.setOnClickListener(this);
        btnDownload.setOnClickListener(this);
        btnNext.setOnClickListener(this);
        btnPrevious.setOnClickListener(this);
        btnShuffle.setOnClickListener(this);

        musicModel = new ViewModelProvider(requireActivity()).get(PlayMusicModel.class);
        mAudioManager = (AudioManager) getContext().getSystemService(getContext().AUDIO_SERVICE);
        handler = new Handler();
    }

    private void mappingView(View view) {
        txtName = view.findViewById(R.id.txt_name);
//        imgViewCD = view.findViewById(R.id.img_cd);

        btnPrevious = view.findViewById(R.id.imgBtn_previous);
        btnNext = view.findViewById(R.id.imgBtn_next);
        btnPlay = view.findViewById(R.id.imgBtn_Play);
        btnDownload = view.findViewById(R.id.imgBtn_dowload);
        btnShuffle = view.findViewById(R.id.shuffle);
        seekbarVolume = view.findViewById(R.id.seekBar_volume);

        circleLineVisualizer = view.findViewById(R.id.circle_line);
        seekbarMusic = view.findViewById(R.id.seekBar_music);

        txtStartTimeSong = view.findViewById(R.id.txt_time_start_song);
        txtEndTimeSong = view.findViewById(R.id.txt_time_end_song);
    }


    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getStringExtra("actionname");

            switch (action) {
                case CreateNotification.ACTION_NEXT: {
                    onTrackNext();
                    break;
                }
                case CreateNotification.ACTION_PLAY: {
                    onTrackPlay();
                    break;
                }
            }
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
//        mediaPlayer.stop();
//        mediaPlayer.release();
//        mediaPlayer = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBtn_Play: {
                if (PlaySongService.mediaPlayer != null) {
                    if (PlaySongService.mediaPlayer.isPlaying()) {
                        PlaySongService.mediaPlayer.pause();
                        btnPlay.setImageResource(R.drawable.ic_play_arrow_24dp);
                        PlaySongService.mData.setValue(new SongRunning(
                                PlaySongService.mediaPlayer.getCurrentPosition(),
                                false
                        ));
                    } else {
                        int l = PlaySongService.mediaPlayer.getCurrentPosition();
                        Log.d(MainActivity.DEBUG_FLAG, l + "");


                        PlaySongService.mediaPlayer.seekTo(l);
                        PlaySongService.mediaPlayer.start();
                        btnPlay.setImageResource(R.drawable.ic_pause_24dp);
                        PlaySongService.mData.setValue(new SongRunning(
                                PlaySongService.mediaPlayer.getCurrentPosition(),
                                true
                        ));
                    }
                } else {
                    Toast.makeText(getContext(), "Pending ...", Toast.LENGTH_SHORT).show();
                }


                break;
            }
        }
    }

    @Override
    public void onTrackPrevious() {
        Toast.makeText(getContext(), "onTrackPrevious", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onTrackPlay() {

        Toast.makeText(getContext(), "onTrackPlay", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onTrackPause() {
        Toast.makeText(getContext(), "onTrackPause", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onTrackNext() {
        Toast.makeText(getContext(), "onTrackNext", Toast.LENGTH_SHORT).show();
    }
}
