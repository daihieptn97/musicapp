package com.hieptran.hellogit.clinet;


import com.hieptran.hellogit.object.JsonObject;
import com.hieptran.hellogit.object.Song;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

public class APIClient {
    private static Retrofit retrofit;

    private static String DOMAIN = "http://192.168.1.181/";
    private static final String BASE_URL = DOMAIN + "musich/";


    public static Retrofit getRetrofit() {
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit;
    }


    public interface APIInterface {
        @GET("api.php?type=vn")
        Call<JsonObject<Song>> getMusicVn();

        @GET("api.php?type=kpop")
        Call<JsonObject<Song>> getMusicKpop();

        @GET("api.php?")
        Call<JsonObject<Song>> getAllMusic();

    }
}
